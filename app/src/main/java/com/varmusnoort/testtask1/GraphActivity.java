package com.varmusnoort.testtask1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.varmusnoort.testtask1.network.model.GraphPoint;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import de.codecrafters.tableview.SortableTableView;
import de.codecrafters.tableview.TableDataAdapter;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;

public class GraphActivity extends AppCompatActivity {

    LineChart lineChart;
    SortableTableView tableView;

    private ArrayList<GraphPoint> points;


    private static final String GRAPH_POINTS = "graph_points";

    public static Intent newIntent(Context packageContext, ArrayList<GraphPoint> graphPoints) {
        Intent intent = new Intent(packageContext, GraphActivity.class);
        intent.putExtra(GRAPH_POINTS, graphPoints);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lineChart = findViewById(R.id.lineChart);
        tableView = findViewById(R.id.tableView);

        points = getPoints();
        Collections.sort(points, new Comparator<GraphPoint>() {
            @Override
            public int compare(GraphPoint o1, GraphPoint o2) {
                return Float.compare(o1.getX(), o2.getX());
            }
        });
        initTable();
        initChart();
    }

    private ArrayList<GraphPoint> getPoints() {
        Intent intent = getIntent();
        ArrayList<GraphPoint> list = intent.getParcelableArrayListExtra(GRAPH_POINTS);
        return list;
    }

    private void initTable() {
        tableView.setColumnCount(2);
        final String[] TABLE_HEADERS = {"X", "Y"};
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, TABLE_HEADERS));

        tableView.setColumnComparator(0, new Comparator<GraphPoint>() {
            @Override
            public int compare(GraphPoint o1, GraphPoint o2) {
                return Float.compare(o1.getX(), o2.getX());
            }
        });
        tableView.setColumnComparator(1, new Comparator<GraphPoint>() {
            @Override
            public int compare(GraphPoint o1, GraphPoint o2) {
                return Float.compare(o1.getY(), o2.getY());
            }
        });
        tableView.setDataAdapter(new PointsTableDataAdapter(this, points));
    }


    private void initChart() {
        ArrayList<GraphPoint> points = getPoints();
        List<Entry> entries = new ArrayList<Entry>();
        for (GraphPoint point : points) {
            entries.add(new Entry(point.getX(), point.getY()));
        }
        LineDataSet dataSet = new LineDataSet(entries, "points");
        dataSet.setColor(getApplicationContext().getResources().getColor(R.color.colorPrimaryDark));
        dataSet.setValueTextColor(getApplicationContext().getResources().getColor(R.color.colorAccent));
        LineData lineData = new LineData(dataSet);
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setDrawLabels(false);
        rightAxis.setDrawGridLines(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.getDescription().setEnabled(false);

        lineChart.setData(lineData);
        lineChart.invalidate();
    }

    public class PointsTableDataAdapter extends TableDataAdapter<GraphPoint> {
        private int paddingLeft = 20;
        private int paddingTop = 15;
        private int paddingRight = 20;
        private int paddingBottom = 15;
        private int textSize = 18;
        private int typeface = Typeface.NORMAL;
        private int textColor = 0x99000000;

        public PointsTableDataAdapter(Context context, List<GraphPoint> data) {
            super(context, data);
        }

        @Override
        public View getCellView(int rowIndex, int columnIndex, ViewGroup parentView) {
            final TextView textView = new TextView(getContext());
            textView.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
            textView.setTypeface(textView.getTypeface(), typeface);
            textView.setTextSize(textSize);
            textView.setTextColor(textColor);
            textView.setSingleLine();
            textView.setEllipsize(TextUtils.TruncateAt.END);

            GraphPoint point = getRowData(rowIndex);
            float value = (columnIndex == 0) ? point.getX() : point.getY();
            textView.setText("" + value);

            return textView;
        }
    }

}
