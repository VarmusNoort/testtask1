package com.varmusnoort.testtask1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.varmusnoort.testtask1.network.model.GraphPoint;
import com.varmusnoort.testtask1.network.interfaces.IGetGraphPointsList;
import com.varmusnoort.testtask1.network.RetrofitRequester;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TextView tvInfoText;
    EditText etPointAmount;
    Button btnRequest;

    RetrofitRequester retrofitRequester = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvInfoText = findViewById(R.id.tvInfoText);
        etPointAmount = findViewById(R.id.etPointAmount);
        btnRequest = findViewById(R.id.btnRequest);

        btnRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String pointsAmountStr = etPointAmount.getText().toString();
                if (pointsAmountStr.length() == 0) {
                    showToast("Укажите количество точек");
                } else {
                    try {
                        int pointAmount = Integer.parseInt(pointsAmountStr);
                        requestGraphPointsFromServer(pointAmount);
                    } catch (NumberFormatException e) {
                        showToast("Введено некорректное количество точек");
                    }
                }
            }
        });

    }

    private void requestGraphPointsFromServer(int pointsAmount) {
        final RetrofitRequester retrofitRequester = new RetrofitRequester(true);
        retrofitRequester.getGraphPoints(pointsAmount, new IGetGraphPointsList() {
            @Override public void onGetGraphPointsListSuccess(ArrayList<GraphPoint> graphPoints) {
                showGraphActivity(graphPoints);
            }
            @Override public void onGetGraphPointsListFailure(String errorMessage) {
                showToast(errorMessage);
            }
        });
    }

    private void showGraphActivity(ArrayList<GraphPoint> graphPoints) {
        Intent intent = GraphActivity.newIntent(MainActivity.this, graphPoints);
        startActivity(intent);
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
