package com.varmusnoort.testtask1.network;

import com.varmusnoort.testtask1.network.requests.GraphPointsRequest;
import com.varmusnoort.testtask1.network.responses.GraphPointsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface AuthAPI {

    @POST("mobws/json/pointsList")
    Call<GraphPointsResponse> getGraphPoints(@Query("count") int count, @Query("version") String email, @Body GraphPointsRequest graphPointsRequest);

}
