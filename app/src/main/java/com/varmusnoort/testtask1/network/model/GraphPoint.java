package com.varmusnoort.testtask1.network.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GraphPoint implements Parcelable {

    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }


    public int describeContents() {
        return 0;
    }

    // упаковываем объект в Parcel
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeFloat(x);
        parcel.writeFloat(y);
    }

    public static final Creator<GraphPoint> CREATOR = new Creator<GraphPoint>() {
        // распаковываем объект из Parcel
        public GraphPoint createFromParcel(Parcel in) {
            return new GraphPoint(in);
        }
        public GraphPoint[] newArray(int size) {
            return new GraphPoint[size];
        }
    };

    // конструктор, считывающий данные из Parcel
    private GraphPoint(Parcel parcel) {
        x = parcel.readFloat();
        y = parcel.readFloat();
    }
}
