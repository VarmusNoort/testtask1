package com.varmusnoort.testtask1.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GraphPointsRequest {


    @SerializedName("version")
    @Expose
    private String version;

    public GraphPointsRequest(String version) {
        this.version = version;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
