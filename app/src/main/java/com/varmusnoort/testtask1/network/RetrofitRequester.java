package com.varmusnoort.testtask1.network;

import android.util.Base64;

import com.varmusnoort.testtask1.network.interfaces.IGetGraphPointsList;
import com.varmusnoort.testtask1.network.requests.GraphPointsRequest;
import com.varmusnoort.testtask1.network.responses.GraphPointsResponse;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;
import static okhttp3.logging.HttpLoggingInterceptor.Level.NONE;


public class RetrofitRequester {

    private String BASE_API_URL = "https://demo.bankplus.ru/";

    private AuthAPI service;
    private boolean showLogs;
    private static HttpLoggingInterceptor.Level LOG_LEVEL;


    public RetrofitRequester(boolean showLogs) {
        this.showLogs = showLogs;
        LOG_LEVEL = (showLogs) ? BODY : NONE;
        initService();
    }

    private void initService() {
        // На сервере, очевидно проблемы с сертификатом, поэтому вообще на него не смотрим
        OkHttpClient okHttpClient = getUnsafeOkHttpClient();
//        OkHttpClient okHttpClient = getSafeOkHttpClient();

        Retrofit client = new Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
        service = client.create(AuthAPI.class);
    }

    public void getGraphPoints(int pointsAmount, final IGetGraphPointsList iGetGraphPointsList) {
        String version = "1.1";
        // Из задания это не очевидно, но запрос отрабатывает только при таком минимальном сочетании параметров: version в body и query-параметром, количество точек query-параметром
        GraphPointsRequest request = new GraphPointsRequest(version);
        Call<GraphPointsResponse> call = service.getGraphPoints(pointsAmount, version, request);
        call.enqueue(new Callback<GraphPointsResponse>() {
            @Override public void onResponse(Call<GraphPointsResponse> call, Response<GraphPointsResponse> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getResult() == 0) {
                        iGetGraphPointsList.onGetGraphPointsListSuccess(response.body().getGraphPoints().getPoints());
                    } else {
                        String errorMessage = getErrorMessage(response.body());
                        iGetGraphPointsList.onGetGraphPointsListFailure(errorMessage);
                    }
                } else {
                    iGetGraphPointsList.onGetGraphPointsListFailure("Не удалось осуществить запрос на сервер");
                }
            }
            @Override public void onFailure(Call<GraphPointsResponse> call, Throwable t) {
                iGetGraphPointsList.onGetGraphPointsListFailure("Не удалось осуществить запрос на сервер");
            }
        });
    }

    private OkHttpClient getSafeOkHttpClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(LOG_LEVEL);
        try {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .addInterceptor(loggingInterceptor)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private OkHttpClient getUnsafeOkHttpClient() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(LOG_LEVEL);
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        @Override public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {}
                        @Override public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {}
                        @Override public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[0];
                        }
                    }
            };
            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .addInterceptor(loggingInterceptor)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .build();
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private String getErrorMessage(GraphPointsResponse response) {
        String defaultErrorMessage = "Непредвиденная ошибка";
        String result = defaultErrorMessage;
        if (response.getResult() == -1 || response.getResult() == -101) { // в задании сказано, что ответ закодирован в Base64, если статус -1, но практика показывает, что при -101 тоже
            byte[] data = Base64.decode(response.getMessage(), Base64.DEFAULT);
            try {
                result = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else {
            result = (response.getMessage() == null || response.getMessage().length() == 0) ? defaultErrorMessage : response.getMessage();
        }
        return result;
    }
}