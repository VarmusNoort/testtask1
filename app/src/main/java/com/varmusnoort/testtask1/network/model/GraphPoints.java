
package com.varmusnoort.testtask1.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class GraphPoints extends BaseResponse {

    @SerializedName("points")
    @Expose
    private ArrayList<GraphPoint> points;

    public ArrayList<GraphPoint> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<GraphPoint> points) {
        this.points = points;
    }

}
