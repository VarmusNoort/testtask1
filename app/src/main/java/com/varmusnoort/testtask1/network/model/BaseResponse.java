package com.varmusnoort.testtask1.network.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @SerializedName("result")
    @Expose
    private int status = -1;

    @SerializedName("message")
    @Expose
    private String message = null;

    public BaseResponse() {
    }

    public BaseResponse(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }


}
