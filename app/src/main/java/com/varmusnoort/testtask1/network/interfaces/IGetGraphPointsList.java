package com.varmusnoort.testtask1.network.interfaces;

import com.varmusnoort.testtask1.network.model.GraphPoint;

import java.util.ArrayList;

public interface IGetGraphPointsList {

    void onGetGraphPointsListSuccess(ArrayList<GraphPoint> graphPoints);

    void onGetGraphPointsListFailure(String errorMessage);
}
