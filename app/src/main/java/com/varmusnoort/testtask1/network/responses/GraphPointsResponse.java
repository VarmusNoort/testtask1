
package com.varmusnoort.testtask1.network.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.varmusnoort.testtask1.network.model.GraphPoints;


public class GraphPointsResponse {

    @SerializedName("result")
    @Expose
    private Integer result = -666; // api кривое - "result" при ошибках то есть, то нет. Чтобы исключить ложно положительные результаты, по дефолту установлю не 0

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("response")
    @Expose
    private GraphPoints graphPoints; // тут может прийти сообщение об ошибке вместо объекта с точками

    public Integer getResult() {
        if (result == -666) {
            return graphPoints.getStatus();
        }
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getMessage() {
        if (result == -666) {
            return graphPoints.getMessage();
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GraphPoints getGraphPoints() {
        return graphPoints;
    }

    public void setGraphPoints(GraphPoints graphPoints) {
        this.graphPoints = graphPoints;
    }
}
